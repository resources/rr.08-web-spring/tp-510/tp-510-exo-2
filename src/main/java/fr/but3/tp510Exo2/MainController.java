package fr.but3.tp510Exo2;

import java.security.Principal;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@Controller
public class MainController {

    @GetMapping(value = "/me")
    public Principal me(@RequestHeader("Authorization") String authorization){
        System.out.println(authorization);
        Authentication auth =  SecurityContextHolder.getContext().getAuthentication();

        return (Principal) auth.getPrincipal();
    }

}
