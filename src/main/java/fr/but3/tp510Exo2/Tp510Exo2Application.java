package fr.but3.tp510Exo2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp510Exo2Application {
   static final Logger log = LoggerFactory.getLogger(Tp510Exo2Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Tp510Exo2Application.class, args);
	}

}
